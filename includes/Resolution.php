<?php
declare(strict_types = 1);

namespace Mireiawen\Lumme;

/**
 * Resolution enum
 *
 * @package Mireiawen\Lumme
 */
enum Resolution: int
{
	case HOUR = 1;
	case DAY = 2;
	case WEEK = 3;
	case MONTH = 4;
}