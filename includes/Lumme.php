<?php
declare(strict_types = 1);

namespace Mireiawen\Lumme;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use IvoPetkov\HTML5DOMDocument;
use JsonException;
use Mireiawen\Lumme\Error\API;
use Mireiawen\Lumme\Error\Credentials;
use function _;
use function json_decode;
use function parse_str;
use function parse_url;
use function sprintf;

class Lumme
{
	/**
	 * Base for Lumme requests
	 *
	 * @var string
	 */
	final protected const BASE_URI_LUMME = 'https://kirjaudu.lumme-energia.fi/auth/realms/Lumme';
	
	/**
	 * Base for Lumme Azure requests
	 *
	 * @var string
	 */
	final protected const BASE_URI_AZURE = 'https://api-omalumme-prod.azure-api.net/FA-OmaLumme-Prod';
	
	/**
	 * Unknown parameter for now
	 *
	 * @var int
	 */
	final protected const REGISTER_TYPE = 2;
	
	/**
	 * Timestamp format used in API
	 *
	 * @var string
	 */
	final protected const API_TIMESTAMP = 'Y-m-d\TH:i:s\Z';
	
	/**
	 * HTTP client
	 *
	 * @var Client
	 */
	protected Client $client;
	
	/**
	 * Customer ID
	 *
	 * @var int
	 */
	protected int $customer_id;
	
	/**
	 * Authorization token
	 *
	 * @var string
	 */
	protected string $token;
	
	public function __construct(int $customer_id = 0, string $token = '')
	{
		$this->client = new Client(
			[
				RequestOptions::COOKIES => TRUE,
				RequestOptions::HEADERS =>
					[
						'Accept' => 'application/json',
					],
			]
		);
		
		$this->customer_id = $customer_id;
		$this->token = $token;
	}
	
	/**
	 * Do a login to the Lumme backend
	 *
	 * @param string $username
	 *    Username
	 *
	 * @param string $password
	 *    Password
	 *
	 * @return string
	 *    The bearer token for the API
	 *
	 * @throws API
	 *    In case of API errors
	 *
	 * @throws Credentials
	 *    In case of wrong credentials are supplied
	 */
	public function Login(string $username, string $password) : string
	{
		// Prepare the query parameters
		$params =
			[
				'client_id' => 'exove-oma-lumme',
				'redirect_uri' => 'https://oma.lumme-energia.fi/',
				'state' => 'abc',
				'response_type' => 'code',
				'scope' => 'openid',
			];
		
		// Request the login page
		try
		{
			$response = $this->client->get(sprintf('%s/protocol/openid-connect/auth', self::BASE_URI_LUMME), [RequestOptions::QUERY => $params]);
		}
		catch (GuzzleException $exception)
		{
			throw new API(_('Request failed'), 0, $exception);
		}
		
		// Parse the response
		$dom = new HTML5DOMDocument();
		try
		{
			if ($dom->loadHTML($response->getBody()->getContents()) === FALSE)
			{
				throw new API(_('DOM parsing failed'), 1);
			}
		}
		catch (Exception $exception)
		{
			if ($exception instanceof API)
			{
				// Just throw our own exception back
				throw $exception;
			}
			
			// Send API exception from parser
			throw new API(_('DOM parsing failed'), 0, $exception);
		}
		
		// Get the login form
		$node = $dom->querySelector('form#kc-form-login');
		if ($node === NULL)
		{
			throw new API(_('DOM parsing failed'), 2);
		}
		
		// Get the login form action URL
		$login_url = $node->getAttribute('action');
		if (empty($login_url))
		{
			throw new API(_('DOM parsing failed'), 3);
		}
		
		// Get the query string from the login URL
		$query = parse_url($login_url, PHP_URL_QUERY);
		if ($query === FALSE || $query === NULL)
		{
			throw new API('Invalid login URL');
		}
		
		// Parse the query string
		$params = [];
		parse_str($query, $params);
		
		// Run the login query
		try
		{
			$response = $this->client->post(sprintf('%s/login-actions/authenticate', self::BASE_URI_LUMME),
				[
					RequestOptions::ALLOW_REDIRECTS => FALSE,
					RequestOptions::QUERY => $params,
					RequestOptions::FORM_PARAMS =>
						[
							'username' => $username,
							'password' => $password,
							'credentialId' => '',
						],
				]
			);
		}
		catch (GuzzleException $exception)
		{
			throw new API(_('Request failed'), 0, $exception);
		}
		
		// If login succeeds, we get redirect
		$locations = $response->getHeader('Location');
		if (count($locations) !== 1)
		{
			throw new Credentials($username);
		}
		
		// Get the query string from the redirect URL
		$query = parse_url($locations[0], PHP_URL_QUERY);
		if ($query === FALSE || $query === NULL)
		{
			throw new API('Invalid redirect URL', 1);
		}
		
		// Parse the query string
		$params = [];
		parse_str($query, $params);
		
		if (!isset($params['code']))
		{
			throw new API('Invalid redirect URL', 2);
		}
		
		$code = $params['code'];
		
		// Run the token query
		try
		{
			$response = $this->client->get(sprintf('%s/oidcAuthorization/', self::BASE_URI_AZURE),
				[
					RequestOptions::HEADERS =>
						[
							'Code' => $code,
						],
				]
			);
		}
		catch (GuzzleException $exception)
		{
			throw new API(_('Request failed'), 0, $exception);
		}
		
		// Parse the response
		try
		{
			$data = json_decode($response->getBody()->getContents(), FALSE, 8, JSON_THROW_ON_ERROR);
		}
		catch (JsonException $exception)
		{
			throw new API(_('Request failed'), 0, $exception);
		}
		
		// Make sure the token and customer ID exist
		if (!isset($data->token, $data->ret->customerId))
		{
			throw new API(_('Token is missing'));
		}
		
		$this->token = $data->token;
		if ($this->customer_id === 0)
		{
			$this->customer_id = $data->ret->customerId;
		}
		return $this->token;
	}
	
	/**
	 * Get the energy consumption report
	 *
	 * @param string $location
	 *    The Lumme Energy usage location id
	 *
	 * @param DateTimeInterface $from
	 *    The reporting period start time
	 *
	 * @param DateTimeInterface $to
	 *    The reporting period end time
	 *
	 * @param Resolution $resolution
	 *    The reporting resolution
	 *
	 * @return Usage[]
	 *    The usage reports for the period
	 *
	 * @throws API
	 *    In case of API failures
	 */
	public function FetchUsage(string $location, DateTimeInterface $from, DateTimeInterface $to, Resolution $resolution = Resolution::HOUR) : array
	{
		// Lumme reports in Europe/Helsinki time zone
		$tz = new DateTimeZone('Europe/Helsinki');
		
		// Create the timestamps for API
		$start_hour = (int)$from->format('G');
		$start = DateTime::createFromInterface($from);
		$start->setTime($start_hour, 0);
		$end_hour = (int)$to->format('G');
		$end = DateTime::createFromInterface($to);
		$end->setTime($end_hour, 59, 59, 999);
		
		// Prepare the parameters
		$params =
			[
				'resolution' => $resolution->value,
				'registerType' => self::REGISTER_TYPE,
				'fromDate' => $start->format(self::API_TIMESTAMP),
				'toDate' => $end->format(self::API_TIMESTAMP),
			];
		
		// Run the token query
		try
		{
			$response = $this->client->get(sprintf('%s/consumption/%d/%s', self::BASE_URI_AZURE, $this->customer_id, $location),
				[
					RequestOptions::HEADERS =>
						[
							'Authorization' => sprintf('Bearer %s', $this->token),
						],
					RequestOptions::QUERY => $params,
				]
			);
		}
		catch (GuzzleException $exception)
		{
			throw new API(_('Request failed'), 0, $exception);
		}
		
		// Parse the response
		try
		{
			$data = json_decode($response->getBody()->getContents(), FALSE, 8, JSON_THROW_ON_ERROR);
		}
		catch (JsonException $exception)
		{
			throw new API(_('Request failed'), 0, $exception);
		}
		
		// Parse the data
		$reports = [];
		foreach ($data as $usage)
		{
			// Make sure the data seems correct
			if (!isset($usage->unit, $usage->directionOfFlow, $usage->fromTime, $usage->toTime, $usage->value))
			{
				continue;
			}
			
			// Report  bought energy only
			if ($usage->directionOfFlow !== 'Out')
			{
				continue;
			}
			
			// Make sure the unit is correct
			if ($usage->unit !== 'kWh')
			{
				continue;
			}
			
			try
			{
				// Create the actual usage report data
				$reports[] = new Usage(new DateTimeImmutable($usage->fromTime, $tz), new DateTimeImmutable($usage->toTime, $tz), $usage->value);
			}
			catch (Exception $exception)
			{
				throw new API(_('Unable to parse date'), 0, $exception);
			}
		}
		
		return $reports;
	}
}
