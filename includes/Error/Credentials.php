<?php
declare(strict_types = 1);

namespace Mireiawen\Lumme\Error;

use JetBrains\PhpStorm\Pure;
use RuntimeException;
use Throwable;
use function _;
use function sprintf;

/**
 * API credentials error exception
 *
 * @package Mireiawen\Lumme\Error
 */
class Credentials extends RuntimeException
{
	/**
	 * The exception constructor
	 *
	 * @param string $message
	 * @param int $code
	 * @param Throwable|null $previous
	 */
	#[Pure] public function __construct(string $message, int $code = 0, ?Throwable $previous = NULL)
	{
		$message = sprintf(_('Invalid credentials for user %s'), $message);
		parent::__construct($message, $code, $previous);
	}
}