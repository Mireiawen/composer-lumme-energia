<?php
declare(strict_types = 1);

namespace Mireiawen\Lumme;

use DateTimeInterface;

/**
 * Usage report class
 *
 * @package Mireiawen\Lumme
 */
class Usage
{
	/**
	 * Period start
	 *
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $from;
	
	/**
	 * Period end
	 *
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $to;
	
	/**
	 * The usage value for the period
	 *
	 * @var float
	 */
	protected float $value;
	
	/**
	 * Create instance of usage period
	 *
	 * @param DateTimeInterface $from
	 *    Period start
	 *
	 * @param DateTimeInterface $to
	 *    Period end
	 *
	 * @param float $value
	 *    The usage value for the usage period
	 */
	public function __construct(DateTimeInterface $from, DateTimeInterface $to, float $value)
	{
		$this->from = $from;
		$this->to = $to;
		$this->value = $value;
	}
	
	/**
	 * Get the period start
	 *
	 * @return DateTimeInterface
	 */
	public function GetFrom() : DateTimeInterface
	{
		return $this->from;
	}
	
	/**
	 * Get the period end
	 *
	 * @return DateTimeInterface
	 */
	public function GetTo() : DateTimeInterface
	{
		return $this->to;
	}
	
	/**
	 * Get the usage for the period
	 *
	 * @return float
	 */
	public function GetValue() : float
	{
		return $this->value;
	}
}