# Lumme Energia
This library helps fetching the energy consumption from the [Lumme Energia](https://www.lumme-energia.fi/) [consumption data](https://oma.lumme-energia.fi/consumption).

The process of login and data fetching has been reverse-engineered by reading the traffic between the browser and the server, and as such the code may or may not work for specific case, and the login and data fetching may change at any time without warning, making this code to not work.

## Requirements
* PHP 8.1
* cURL
* JSON
* Gettext

## Installation
```bash
composer require "mireiawen/lumme-energia"
```

## Simple example
```php
<?php
// Username and password to access Oma Lumme
$username = 'root@localhost';
$password = 'REALLY_SECRET_PASSWORT!';

// Usage location number (Käyttöpaikkanumero), available at Oma Lumme
$location = '12334567890';

// Create time zone
$tz = new \DateTimeZone('Europe/Helsinki');

// Create the start and end times, get last 6 days of data
$start = new \DateTime('now', $tz);
$start->modify('- 6 days');
$end = new \DateTime('now', $tz);
	
// Create the instance
$lumme = new Lumme();

// Log in
// The method returns the token if you wish to use it elsewhere
$lumme->Login($username, $password);

// Fetch the usage data
$usage = $lumme->FetchUsage($loc, $start, $end);

// The data is array of Usage objects
// Usage has following methods: GetFrom(), GetTo(), GetValue()
var_dump($usage);
```
